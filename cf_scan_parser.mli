(*---------------------------------------------------------------------------*
  INTERFACE  cf_scan_parser.mli

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Lexical analysis with functional composition using [Scanf] scanners. *)

(** {6 Overview}

    This module implements and extension to the {!Cf_parser} module for mixing
    calls to the standard library [Scanf] functions with functional parsers.
*)

(** {6 Classes and Types} *)

(** An exception provided so that the [cscanf] function (below) can be signaled
    to transform its answer with the effect that the parser stack is unwound
    until an alternative production can be matched.
*)
exception No_match

(** A virtual base class used in the [cscanf] function (below) for constructing
    a scanning buffer from an input sequence.
*)
class virtual ['i] scanner:
    'i Cf_seq.t ->      (** The input sequence *)
    object
        val mutable next_: 'i Cf_seq.t  (** The next unmatched input symbol *)

        (** Get the next character for the scanning buffer *)
        method private virtual get: char
        
        (** Initialize the scanning buffer *)
        method init: Scanf.Scanning.scanbuf
        
        (** Finalize the scanning buffer and return the next unmatched input
            symbol.
        *)
        method fini: 'i Cf_seq.t
    end

(** {6 Functions} *)

(** This is the primitive function in the module.  Use [cscanf cf ef fmt rf] to
    construct a parser that applies [cf] to the input sequence to acquire a
    scanner object [s], invokes the [s#init] method to obtain a scanning buffer
    with which to apply [Scanf.kscanf], using the exception function [ef], the
    scanning format [fmt] and the return continuation [rf].  If the exception
    function raises [No_match] then the resulting parser unwinds to the next
    production alternative, otherwise the parser answers with the result of the
    return continuation.
*)
val cscanf:
    ('i Cf_seq.t -> ('i #scanner as 's)) -> ('s -> exn -> 'o) ->
    ('f, Scanf.Scanning.scanbuf, 'u, 'f -> 'o, 'f -> 'o, 'o) format6 -> 'f ->
    ('i, 'o) Cf_parser.t

(** Use [scanf fmt rf] to construct a lexical parser that scans the input text
    according to the scanning format [fmt] and produces the value returned by
    the return continuation.  If the scanner raises an exception, then the
    parser unwinds to the next production alternative.
*)
val scanf:
    ('f, Scanf.Scanning.scanbuf, 'u, 'f -> 'o, 'f -> 'o, 'o) format6 -> 'f ->
    (char, 'o) Cf_parser.t

(** Use [scanfx] in place of [scanf] to construct a parser with a cursor weaved
    into the input stream.
*)
val scanfx:
    ('f, Scanf.Scanning.scanbuf, 'u, 'f -> 'o, 'f -> 'o, 'o) format6 -> 'f ->
    (char #Cf_parser.cursor, char, 'o) Cf_parser.X.t

(*--- End of File [ cf_scan_parser.mli ] ---*)
