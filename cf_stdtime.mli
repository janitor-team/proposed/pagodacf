(*---------------------------------------------------------------------------*
  INTERFACE  cf_stdtime.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Conversions between Standard Time, UTC and TAI. *)

(** {6 Overview} *)

(** This module contains a type for representing values of standard time,
    defined as a reference from Coordinated Universal Time (UTC).  It also
    provides functions for converting values of standard time to and from
    TAI64 values (see {!Cf_tai64}).
    
    {b Note:} no facility is yet provided here for manipulating time with
    associated timezone attributes.  Converting arbitrary values of UTC time
    to and from local time is tricky, since daylight savings time rules vary
    greatly from locality to locality and also over time.
    
    {b Warning!} The International Earth Rotation and Reference System Service
    determines whether and when to insert or delete leap seconds into the UTC
    timescale.  These announcements are made every six months in an IERS
    bulletin.  The current implementation contains a hard-coded table of leap
    seconds that was accurate at least until Jan 1, 2007.
*)

(** {6 Types} *)

(** A record with field for each component of a standard time value. *)
type t = {
    year: int;    	(** -2937947 .. 2941664, 1 BC == 0 *)
    month: int;    	(** january=1 .. december=12 *)
    day: int;    	(** 1 .. 31 *)
    hour: int;    	(** 0 .. 23 *)
    minute: int;    (** 0 .. 59 *)
    second: int;    (** 0 .. 60, 60=leap second *)
}

(** {6 Functions} *)

(** Convert from TAI64 to UTC time.  Use [utc_of_tai64 ~wday ~yday tai] to
    obtain a UTC standard time record corresponding to the TAI time index
    [tai].  Optionally, if ~wday and/or ~yday are provided, this function
    stores the day of the week (0=sunday,6=saturday) in [wday] and the day of
    year (1..366) in [yday].
*)
val utc_of_tai64: ?wday:int ref -> ?yday:int ref -> Cf_tai64.t -> t

(** Convert from UTC time to TAI64.  Use [utc ~year ~month ~day ~hour ~minute
    ~sec] to obtain a TAI64 value corresponding to the UTC standard time
    specified by the [~year ~month ~day ~hour ~minute ~sec] arguments.  Raises
    [Invalid_argument] if the time specified is not a valid UTC time value.
*)
val utc_to_tai64:
    year:int -> month:int -> day:int -> hour:int -> minute:int ->
    second:int -> Cf_tai64.t

(** This is the same as [utc_to_tai64], except it assumes the input is a valid
    UTC time value.  If it isn't, then the result is undefined.
*)
val utc_to_tai64_unsafe:
    year:int -> month:int -> day:int -> hour:int -> minute:int ->
    second:int -> Cf_tai64.t

(*--- End of File [ cf_stdtime.mli ] ---*)
