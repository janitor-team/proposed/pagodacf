(*---------------------------------------------------------------------------*
  INTERFACE  cf_machine.mli

  Copyright (c) 2005-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Object-oriented framework for monadic composition of complex stream
    processors.
*)

(** {6 Overview}
    
    This module extends {!Cf_state_gadget} with an object-oriented framework.
    The idea is to facilitate reuse of gadget monads among related processes by
    class inheritance.
*)

(** Use [inherit \['i, 'o\] start] to derive a class to represent the
    initial state of a machine.  It's [start] method initiates the machine
    with the virtual private [guard] method.
*)
class virtual ['i, 'o] start:
    object('self)
        (** The first guard evaluationed by the machine after starting. *)
        method virtual private guard:
            ('self, 'i, 'o, unit) Cf_state_gadget.guard
        
        (** Starts a new gadget process.  Defined as {!Cf_state_gadget.start}
            [Cf_state_gadget.guard self#guard self].
        *)
        method start: 's. ('s, 'i, 'o, unit) Cf_state_gadget.t
    end

(** Use [inherit \['i, 'o\] next] to derive a class that implements an
    intermediate state in a machine.
*)
class virtual ['i, 'o] next:
    object('self)        
        (** The guard evaluated by this state of the machine. *)
        method virtual private guard:
            ('self, 'i, 'o, unit) Cf_state_gadget.guard

        (** Use [obj#next] to transition the state of the machine by storing
            [obj] in the state of the gadget and applying
            {!Cf_state_gadget.guard} [self#guard].
        *)
        method next: 'a. ('self, 'i, 'o, 'a) Cf_state_gadget.t
    end

(*--- End of File [ cf_machine.mli ] ---*)
