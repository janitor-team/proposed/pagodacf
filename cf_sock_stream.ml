(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_sock_stream.ml

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

module type T = sig
    include Cf_sock_common.T with type P.ST.tag = [ `SOCK_STREAM ]

    class initiator:
        ?sock:t -> ?src:address -> address ->
        object
            inherit basic
            
            method virtual getsockname: address
            method connect: unit
        end

    class listener:
        ?sock:t -> address ->
        object
            inherit basic
            
            method virtual getsockname: address
            method listen: int -> unit
            method accept: t * address
        end

    class endpoint:
        t ->
        object('self)
            inherit basic
            constraint 'self = #Cf_sock_common.endpoint
            
            method virtual getsockname: address
            method virtual getpeername: address

            method send:
                ?flags:Cf_socket.msg_flags -> string -> int -> int -> int
            method recv:
                ?flags:Cf_socket.msg_flags -> string -> int -> int -> int

            method virtual shutdown: Unix.shutdown_command -> unit
        end
end

module Create(P: Cf_socket.P with module ST = Cf_socket.SOCK_STREAM) = struct
    include Cf_sock_common.Create(P)
    
    module S0 = Cf_socket
    
    class initiator ?sock ?src dst =
        let src =
            match src with
            | Some a -> P.AF.to_sockaddr a
            | None -> P.AF.unspecified
        in
        let dst = P.AF.to_sockaddr dst in
        object
            inherit basic ?sock ()
            
            method virtual getsockname: address

            method connect = S0.connect socket_ dst
            
            initializer
                S0.bind socket_ src
        end
    
    class listener ?sock src =
        object
            inherit basic ?sock () as super
            
            method virtual getsockname: address

            method listen = S0.listen socket_
            
            method accept =
                let socket, address = S0.accept socket_ in
                socket, (P.AF.of_sockaddr address)

            initializer
                S0.bind socket_ (P.AF.to_sockaddr src)
        end
    
    class endpoint sock =
        object
            inherit basic ~sock ()
            
            method virtual getsockname: address
            method virtual getpeername: address
            
            method virtual shutdown: Unix.shutdown_command -> unit
            
            method send ?(flags = S0.msg_flags_none) msg pos len =
                S0.send socket_ msg pos len flags
            
            method recv ?(flags = S0.msg_flags_none) msg pos len =
                S0.recv socket_ msg pos len flags
            
        end
end

(*--- End of File [ cf_sock_stream.ml ] ---*)
