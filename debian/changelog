pagodacf (0.10-5) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Sat, 07 Sep 2019 20:35:47 +0200

pagodacf (0.10-4) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Switch source package format to 3.0 (quilt)
  * Update Vcs-*
  * Bump debhelper compat to 10

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

  [ Dimitri John Ledkov ]
  * Fix ftbfs with ocaml 4.04.

 -- Stéphane Glondu <glondu@debian.org>  Wed, 19 Jul 2017 10:54:15 +0200

pagodacf (0.10-3) unstable; urgency=low

  * Use dh-ocaml 0.9.1 features
  * Upgrade Standards-Version to 3.8.3 (section ocaml, README.source)

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 15 Dec 2009 21:18:24 +0000

pagodacf (0.10-2) unstable; urgency=low

  * Rebuild with OCaml 3.11.
  * Switch packaging to git.
  * Use dh-ocaml.
  * Update compat to 7.

 -- Samuel Mimram <smimram@debian.org>  Sun, 01 Mar 2009 21:00:31 +0100

pagodacf (0.10-1) unstable; urgency=low

  * New upstream release.
  * Updated standards version to 3.8.0, no changes needed.

 -- Samuel Mimram <smimram@debian.org>  Mon, 14 Jul 2008 14:58:23 +0200

pagodacf (0.9-2) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Stephane Glondu ]
  * Fix FTBFS: The implementation cf_ip4_addr.ml does not match the
    interface cf_ip4_addr.cmi (Closes: #482448).
  * Add Homepage field to debian/control.
  * Update Standards-Version to 3.7.3 (no changes required).

  [ Sylvain Le Gall ]
  * Rebuild for ocaml 3.10.2

 -- Sylvain Le Gall <gildor@debian.org>  Fri, 23 May 2008 14:10:57 +0200

pagodacf (0.9-1) unstable; urgency=low

  * New upstream release.
  * Removed scan_parser.dpatch, integrated upstream.
  * Fixes a bug in Cf_rbtree.Map.nearest_incr, closes: #432590.

 -- Samuel Mimram <smimram@debian.org>  Wed, 19 Sep 2007 09:58:09 +0000

pagodacf (0.8-2) unstable; urgency=low

  * Rebuild with OCaml 3.10.
  * Added scan_parser.dpatch to fix the FTBFS with OCaml 3.10,
    closes: #441645.
  * Make the package binNMUable.
  * Don't ignore errors on clean.

 -- Samuel Mimram <smimram@debian.org>  Tue, 18 Sep 2007 22:18:45 +0000

pagodacf (0.8-1) unstable; urgency=low

  * New upstream release.

 -- Samuel Mimram <smimram@debian.org>  Thu, 20 Jul 2006 19:36:06 +0000

pagodacf (0.7-4) unstable; urgency=low

  * Rebuild with OCaml 3.09.2.
  * Updated standards version to 3.7.2, no changes needed.

 -- Samuel Mimram <smimram@debian.org>  Thu, 18 May 2006 22:47:34 +0000

pagodacf (0.7-3) unstable; urgency=low

  * Rebuild with OCaml 3.09.1.
  * Handling OCaml's ABI changes in package.

 -- Samuel Mimram <smimram@debian.org>  Sun,  8 Jan 2006 18:32:06 +0100

pagodacf (0.7-2) unstable; urgency=low

  * Updated to OCaml 3.09.0.
  * Compiling without -principal option in order to avoid a bug in the
    compiler.
  * Updated watch file.

 -- Samuel Mimram <smimram@debian.org>  Wed,  9 Nov 2005 19:58:58 +0100

pagodacf (0.7-1) unstable; urgency=low

  * New upstream release.
  * Updated standards version to 3.6.2.

 -- Samuel Mimram <smimram@debian.org>  Tue, 26 Jul 2005 11:12:43 +0200

pagodacf (0.6-1) unstable; urgency=low

  * New upstream release.
  * Updated watch file.

 -- Samuel Mimram <smimram@debian.org>  Thu,  2 Jun 2005 14:38:54 +0200

pagodacf (0.5-2) unstable; urgency=medium

  * Updated to OCaml 3.08.3.
  * Set Debian OCaml Maintainers as Maintainer, put myself in Uploaders.

 -- Samuel Mimram <smimram@debian.org>  Thu, 24 Mar 2005 19:31:38 +0100

pagodacf (0.5-1) unstable; urgency=low

  * New upstream release.
  * Installing *.h files.
  * Updated upstream website (in copyright and watch).
  * Changed my email since I'm a DD now.

 -- Samuel Mimram <smimram@debian.org>  Sat,  1 Jan 2005 16:50:13 +0100

pagodacf (0.4-1) unstable; urgency=low

  * New upstream release.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Sun, 10 Oct 2004 23:14:20 +0200

pagodacf (0.3-4) unstable; urgency=low

  * The install does not depend anymore on files needing ocamlopt to be built.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Thu, 26 Aug 2004 00:50:55 +0200

pagodacf (0.3-3) unstable; urgency=low

  * Set LDCONF to ignore which should solve the last build problems.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Mon, 23 Aug 2004 10:33:32 +0200

pagodacf (0.3-2) unstable; urgency=low

  * Use ocamldoc instead of ocamldoc.opt which was not installed.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Thu, 19 Aug 2004 11:29:35 +0200

pagodacf (0.3-1) unstable; urgency=low

  * Initial Release, closes: #266168.

 -- Samuel Mimram <samuel.mimram@ens-lyon.org>  Tue, 17 Aug 2004 18:14:36 +0200
