/*---------------------------------------------------------------------------*
  C HEADER  cf_common_p.h

  Copyright (c) 2002-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*/

#ifndef _CF_COMMON_P_H
#define _CF_COMMON_P_H

#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/intext.h>
#include <caml/memory.h>
#include <caml/misc.h>
#include <caml/mlvalues.h>

CAMLextern void enter_blocking_section(void);
CAMLextern void leave_blocking_section(void);

typedef value (*Cf_const_constructor_f)(int n);

struct cf_constant_table_s {
    const int*			array;
    unsigned int		size;
    Cf_const_constructor_f	unknown;
};

typedef struct cf_constant_table_s Cf_constant_table_t;

extern value cf_get_constant(const Cf_constant_table_t* tablePtr, int n);

#define Nothing		((value) 0)

/*--- These are imported from the unix library in the ocaml distro ---*/
extern void unix_error(int code, const char fname[], value arg) Noreturn;
extern void uerror(const char* fname, value arg) Noreturn;

#endif /* defined(_CF_COMMON_P_H) */

/*--- End of File [ cf_common_p.h ] ---*/
