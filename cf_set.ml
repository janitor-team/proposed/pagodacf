(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_set.ml

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

module type T = sig
    type t

    module Element: sig type t end
        
    val nil: t
    val empty: t -> bool
    val size: t -> int
    val member: Element.t -> t -> bool
    
    val singleton: Element.t -> t

    val min: t -> Element.t
    val max: t -> Element.t
    
    val put: Element.t -> t -> t
    val clear: Element.t -> t -> t

    val union: t -> t -> t
    val diff: t -> t -> t
    val intersect: t -> t -> t
    val compare: t -> t -> int
    val subset: t -> t -> bool
    
    val of_list: Element.t list -> t
    val of_list_incr: Element.t list -> t
    val of_list_decr: Element.t list -> t
    
    val of_seq: Element.t Cf_seq.t -> t
    val of_seq_incr: Element.t Cf_seq.t -> t
    val of_seq_decr: Element.t Cf_seq.t -> t

    val to_list_incr: t -> Element.t list
    val to_list_decr: t -> Element.t list

    val to_seq_incr: t -> Element.t Cf_seq.t
    val to_seq_decr: t -> Element.t Cf_seq.t
    
    val nearest_decr: Element.t -> t -> Element.t Cf_seq.t
    val nearest_incr: Element.t -> t -> Element.t Cf_seq.t
    
    val iterate: (Element.t -> unit) -> t -> unit
    val predicate: (Element.t -> bool) -> t -> bool
    val fold: ('a -> Element.t -> 'a) -> 'a -> t -> 'a
    val filter: (Element.t -> bool) -> t -> t
    val partition: (Element.t -> bool) -> t -> t * t
end

(*--- End of File [ cf_set.ml ] ---*)
