(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_lexer.ml

  Copyright (c) 2002-2005, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(*
module J = Cf_journal
let jout = J.stdout
*)

module Symbol = struct
    type t = char and 'a map = 'a array
    let map f = Array.init 256 (fun n -> f (char_of_int n))
    let get m c = Array.unsafe_get m (int_of_char c)
end

module DFA = Cf_dfa.Create(Symbol)

class cursor pos =
    object(_:'self)
        inherit [char] Cf_parser.cursor pos
        
        method error (_: int) (_: (char * 'self) Cf_seq.t) = ()
    end

type expr_t = DFA.x
type ('c, 'x) rule_t = ('c, 'x) DFA.X.r constraint 'c = #cursor
type ('c, 'x) t = ('c, 'x) DFA.X.t constraint 'c = #cursor

module Op = struct
    include DFA.X.Op
    
    let ( !$ ) str = !~ (Cf_seq.of_string str)
    let ( $^ ) e f = e $> (fun z -> f (Cf_seq.to_string z))
end

open DFA.Op

let nil = DFA.nil
let create dfa = DFA.X.create dfa

type counter_t = {
    c_pos: int;
    c_row: int;
    c_col: int;
}

let counter_zero = {
    c_pos = 0;
    c_row = 0;
    c_col = 0;
}

class line_cursor ?(c = counter_zero) newline =
    let nl0 = Cf_seq.to_list (Cf_seq.of_string newline) in
    object(self:'self)
        inherit cursor c.c_pos

        val row_: int = c.c_row
        val col_: int = c.c_col
        val nlz_: char list = nl0
        val nl0_: char list = nl0
        
        method counter = {
            c_pos = position_;
            c_row = row_;
            c_col = col_;
        }
        
        method row = row_
        method col = col_
        
        method private next ch =
            match nlz_ with
            | hd :: [] when ch = hd -> succ row_, 0, nl0_
            | hd :: tl when ch = hd -> row_, succ col_, tl
            | _ -> row_, succ col_, nlz_
        
        method advance ch =
            let row, col, nlz = self#next ch in {<
                position_ = succ position_;
                row_ = row;
                col_ = col;
                nlz_ = nlz_
            >}
    end

exception Error of counter_t

let raise_exn n s =
    Error begin
        match Lazy.force (Cf_seq.shift n s) with
        | Cf_seq.Z -> assert (not true); counter_zero
        | Cf_seq.P ((_, cursor), _) -> cursor#counter
    end

(*--- End of File [ cf_lexer.ml ] ---*)
