(*---------------------------------------------------------------------------*
  INTERFACE  cf_lex.mli

  Copyright (c) 2005-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Lexical analysis with functional composition of regular grammars. *)

(** {6 Overview}

    This module implements functional parsers on octet character sequences
    using regular expressions and functional composition of lazy deterministic
    finite automata.
*)

(** {6 Core Interface} *)

(** The type of regular expressions. *)
type x = Cf_regex.DFA.x

(** The type of lexical analysis rules. *)
type 'a r = 'a Cf_regex.DFA.r

(** Character stream parser. *)
type 'a t = (char, 'a) Cf_parser.t

(** Epsilon, i.e. a subexpression that matches an empty input sequence. *)
val nil: x

(** Use [create r] to compose a lexical analyzer from the rule [r]. *)
val create: 'a r -> 'a t

(** The module type containing the subexpression composition operators.  This
    module type is included in the signatures of the [Op] and [X.Op] modules.
*)
module type Expr_Op_T = sig

    (** Alternating composition.  Use [a $| b] to compose an expression that
        matches either expression [a] or expression [b].
    *)
    val ( $| ): x -> x -> x
    
    (** Serial composition.  Use [a $& b] to compose an expression that matches
        expression [a] followed by expression [b].
    *)
    val ( $& ): x -> x -> x
    
    (** Star composition.  Use [!*a] to compose an expression that matches zero
        or any number of instances of [a].
    *)
    val ( !* ): x -> x
    
    (** Plus composition.  Use [!+a] to compose an expression that matches one
        or more instances of [a].
    *)
    val ( !+ ): x -> x
    
    (** Optional composition.  Use [!?a] to compose an expression that matches
        zero or one instance of [a].
    *)
    val ( !? ): x -> x
    
    (** Character literal.  Use [!:c] to compose an expression that matches the
        character [c].
    *)
    val ( !: ): char -> x
    
    (** Character set.  Use [!^f] to compose an expression that matches any
        character for which the satisfier function [f] returns [true].
    *)
    val ( !^ ): (char -> bool) -> x
    
    (** Regular expression sequence.  Use [!~z] to parse the sequence [z]
        according to the grammar defined in {!Cf_regex} module and compose
        an expression that matches input accordingly.  Raises {!Cf_regex.Error}
        if the sequence is not a regular expression.
    *)
    val ( !~ ): char Cf_seq.t -> x
    
    (** Regular expression string.  Use [!~s] to parse the string [s] according
        to the grammar defined in {!Cf_regex} module and compose an expression
        that matches input accordingly.  Raises {!Cf_regex.Error} if the string
        is not a regular expression.
    *)
    val ( !$ ): string -> x
end

(** Open this module to bring the operator functions for simple parsers into
    the current scope.
*)
module Op: sig

    (** Include the expression operators common among lexical analyzers. *)
    include Expr_Op_T
    
    (** Literal token rule.  Use [e $= obj] to compose a rule that outputs the
        literal object [obj] when the expression [e] is recognized.
    *)
    val ( $= ): x -> 'a -> 'a r
    
    (** String token rule.  Use [e $> f] to compose a rule that applies the
        string recognized by the expression [e] to the tokenizer function [f]
        to produce its result.
    *)
    val ( $> ): x -> (string -> 'a) -> 'a r
    
    (** Advanced token rule.  Use [e $@ f] to compose a rule that applies the
        length of the character sequence recognized by the expression [e] to
        the advanced tokenizer function [f] to obtain a parser that produces
        the output of the rule and makes any other manipulations necessary to
        continue parsing the input stream.  If the parser returned by [f] does
        not recognize the input, then no output is produced and no other rules
        are matched.
    *)
    val ( $@ ): x -> (int -> 'a t) -> 'a r

    (** Rule aggregation.  Use this operator to combine a list of rules into a
        single rule.
    *)
    val ( !@ ): 'a r list -> 'a r
    
    (** String parser.  Use [?~x] to create a simple parser that recognizes any
        string that matches the expression [x].  {b Note:} Care should be taken
        when composing parsers with this operator to keep the lazy DFA from
        being recreated in every pass.
    *)
    val ( ?~ ): x -> string t
    
    (** String parser.  Use [?$s] to create a simple parser that recognizes any
        string that matches the regular expression specified in the string [s]
        according to the grammar in the {!Cf_regex} module.  Raises
        {!Cf_regex.Error} if the string is not a regular expression.  {b Note:}
        Care should be taken when composing parsers with this operator to keep
        from parsing the argument string in every pass.
    *)
    val ( ?$ ): string -> string t
end

(** A module of parser extensions for working with input sequences that require
    position information to woven into the parse function.
*)
module X: sig
    
    (** The type of lexical analysis rules. *)
    type ('c, 'a) r constraint 'c = char #Cf_parser.cursor

    (** Woven character stream parser. *)
    type ('c, 'a) t = ('c, char, 'a) Cf_parser.X.t
        constraint 'c = char #Cf_parser.cursor

    (** Use [create r] to compose a lexical analyzer from the rule [r]. *)
    val create: ('c, 'a) r -> ('c, 'a) t

    (** Open this module to bring the operator functions for woven parsers into
        the current scope.
    *)
    module Op: sig

        (** Include the expression operators common among lexical analyzers. *)
        include Expr_Op_T
        
        (** Literal token rule.  Use [e $= obj] to compose a rule that outputs
            the literal object [obj] when the expression [e] is recognized.
        *)
        val ( $= ): x -> 'a -> ('c, 'a) r

        (** String token rule.  Use [e $> f] to compose a rule that applies the
            string recognized by the expression [e] to the tokenizer function
            [f] to produce its result.
        *)
        val ( $> ): x -> (string -> 'a) -> ('c, 'a) r

        (** Advanced token rule.  Use [e $@ f] to compose a rule that applies
            the length of the character sequence recognized by the expression
            [e] to the advanced tokenizer function [f] to obtain a parser that
            produces the output of the rule and makes any other manipulations
            necessary to continue parsing the input stream.  If the parser
            returned by [f] does not recognize the input, then no output is
            produced and no other rules are matched.
        *)
        val ( $@ ): x -> (int -> ('c, 'a) t) -> ('c, 'a) r

        (** Rule aggregation.  Use this operator to combine a list of rules
            into a single rule.
        *)
        val ( !@ ): ('c, 'a) r list -> ('c, 'a) r
        
        (** String parser.  Use [?~x] to create a simple parser that recognizes
            any string that matches the expression [x].  {b Note:} Care should
            be taken when composing parsers with this operator to keep the lazy
            DFA from being recreated in every pass.
        *)
        val ( ?~ ): x -> ('c, string) t
    
        (** String parser.  Use [?$s] to create a simple parser that recognizes
            any string that matches the regular expression specified in the
            string [s] according to the grammar in the {!Cf_regex} module.
            Raises {!Cf_regex.Error} if the string is not a regular expression.
            {b Note:} Care should be taken when composing parsers with this
            operator to keep from parsing the argument string in every pass.
        *)
        val ( ?$ ): string -> ('c, string) t
    end
end

(** A record used by the [cursor] class defined below that indicates the
    character index, row and column in the input stream associated with a
    cursor position.
*)
type counter = {
    c_pos: int;     (** The character index (counts from zero). *)
    c_row: int;     (** The column number (counts from zero). *)
    c_col: int;     (** The row number (counts from zero). *)
}

(** The initial value of a cursor position counter. *)
val counter_zero: counter

(** A class derived from {!Cf_parser.cursor} that intercepts newline characters
    to track the row and column of a cursor position.  Use [new cursor ~c s] to
    construct an initial cursor position, optionally with the counter [c]
    (default: [counter_zero]), and a string [s] containing the character
    sequence that is recognized as a newline, e.g. "\013\010" indicates that
    newline is a CR LF sequence.
*)
class cursor:
    ?c:counter ->
    string ->
    object
        inherit [char] Cf_parser.cursor
        
        val row_: int           (** The current row number *)
        val col_: int           (** The current column number *)
        val nl0_: char list     (** The newline sequence as a [char list]. *)
        val nlz_: char list     (** The current tail of the newline. *)

        (** [self#next c] is called in the [advance] method to return a new
            values for the [row_], [col_] and [nlz_] members.
        *)
        method private next: char -> int * int * char list
        
        (** Returns a new counter object containing the row, column and index
            of the current cursor position.
        *)
        method counter: counter
        
        (** Returns the [row_] member. *)
        method row: int
        
        (** Returns the [col_] member. *)
        method col: int
    end

(*--- End of File [ cf_lex.mli ] ---*)
