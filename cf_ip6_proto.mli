(*---------------------------------------------------------------------------*
  INTERFACE  cf_ip6_proto.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** The [AF_INET6] address family (for use with TCP and UDP over IPv6). *)

(** {6 Modules and Types} *)

(** The address family module for IPv6 transports. *)
module AF: Cf_socket.AF with
    type tag = [ `AF_INET6 ] and
    type address = Cf_ip6_addr.opaque Cf_ip6_addr.t * int * int32

(** The TCP (IPv6) socket protocol. *)
module TCP: Cf_socket.P with
    module AF = AF and
    module ST = Cf_socket.SOCK_STREAM

(** The UDP (IPv6) socket protocol. *)
module UDP: Cf_socket.P with
    module AF = AF and
    module ST = Cf_socket.SOCK_DGRAM

(** The multicast request type *)
type mreq = {
    ipv6mr_multiaddr: Cf_ip4_addr.multicast Cf_ip4_addr.t;
    ipv6mr_interface: int;
}

(** {6 Socket Options}

    The following socket options are available on sockets of AF_INET6 family.
*)

(** Set the unicast hop count for the socket. *)
val ipv6_unicast_hops: (int, [ `AF_INET6 ], 'st) Cf_socket.sockopt

(** Set the unicast hop count for the socket. *)
val ipv6_v6only: (bool, [ `AF_INET6 ], 'st) Cf_socket.sockopt

(** Add the socket to the membership of a multicast group. *)
val ipv6_join_group: (mreq, [ `AF_INET6 ], [ `SOCK_DGRAM ]) Cf_socket.sockopt

(** Drop the socket from the membership of a multicast group. *)
val ipv6_leave_group: (mreq, [ `AF_INET6 ], [ `SOCK_DGRAM ]) Cf_socket.sockopt

(** The primary network interface address for sending to multicast
    destinations.
*)
val ipv6_multicast_if: (int, [ `AF_INET6 ], [ `SOCK_DGRAM ]) Cf_socket.sockopt

(** The multicast hop count for the socket. *)
val ipv6_multicast_hops:
    (int, [ `AF_INET6 ], [ `SOCK_DGRAM ]) Cf_socket.sockopt

(** Enable multicast loopback on the socket. *)
val ipv6_multicast_loop:
    (bool, [ `AF_INET6 ], [ `SOCK_DGRAM ]) Cf_socket.sockopt

(*--- End of File [ cf_ip6_proto.mli ] ---*)
