(*---------------------------------------------------------------------------*
  INTERFACE  cf_ip_common.mli

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** The well-known protocol identifiers for IPv4 and IPv6 protocols. *)

(** The unspecified Internet protocol identifier. *)
val zero: Cf_socket.protocol

(** The Internet Control and Management Protocol. *)
val icmp: Cf_socket.protocol

(** The Internet Protocol (version 4) for tunneling. *)
val ipv4: Cf_socket.protocol

(** The Internet Protocol (version 6) for tunneling. *)
val ipv6: Cf_socket.protocol

(** The Internet Transmission Control Protocol. *)
val tcp: Cf_socket.protocol

(** The Internet User Datagram Protocol. *)
val udp: Cf_socket.protocol

(** {6 Socket Options}

    The following socket options are available on sockets of AF_INET and
    AF_INET6 address/protocol families.
*)

(** Disables the Nagle algorithm for TCP connections. *)
val tcp_nodelay:
    (bool, [< `AF_INET | `AF_INET6 ], [ `SOCK_STREAM ]) Cf_socket.sockopt

(*--- End of File [ cf_ip_common.mli ] ---*)
