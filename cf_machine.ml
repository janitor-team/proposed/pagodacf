(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_machine.ml

  Copyright (c) 2005-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

open Cf_cmonad.Op

class virtual ['i, 'o] start =
    object(self:'self)
        method private virtual guard:
            ('self, 'i, 'o, unit) Cf_state_gadget.guard
        
        method start: 's. ('s, 'i, 'o, unit) Cf_state_gadget.t =
            Cf_state_gadget.start (Cf_state_gadget.guard self#guard) self
    end

class virtual ['i, 'o] next =
    object(self:'self)
        method private virtual guard:
            ('self, 'i, 'o, unit) Cf_state_gadget.guard

        method next: 'a. ('self, 'i, 'o, 'a) Cf_state_gadget.t =
            Cf_state_gadget.store self >>= fun () ->
            Cf_state_gadget.guard self#guard
    end

(*--- End of File [ cf_machine.ml ] ---*)
