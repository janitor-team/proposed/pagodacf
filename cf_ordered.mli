(*---------------------------------------------------------------------------*
  INTERFACE  cf_ordered.mli

  Copyright (c) 2002-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

(** Ordered types.  A functor for composing key-value pairs using an associated
    total ordering function.  This is used in data structure modules like
    {!Cf_rbtree} and {!Cf_sbheap}.
*)

(** The module type defining a type [t] and a corresponding function to give
    the total order of all values of that type. *)
module type Total_T = sig

    (** An abstract type *)
    type t

    (** [compare a b] compares the total ordering of [a] and [b], and it
        returns zero if the two values are of equal total order, a negative
        integer if [a] follows [b], and a positive integer if [a] precedes
        [b] in the total ordering.  The [Pervasives.compare] function is
        a suitable value in modules of this type.
    *)
    val compare: t -> t -> int
end

(** The order of integers. *)
module Int_order: Total_T with type t = int

(*--- End of File [ cf_ordered.mli ] ---*)
