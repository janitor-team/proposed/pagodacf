(*---------------------------------------------------------------------------*
  IMPLEMENTATION  cf_scan_parser.ml

  Copyright (c) 2004-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*)

exception No_match

class virtual ['i] scanner z =
    object(self)
        val mutable next_: 'i Cf_seq.t = z

        method private virtual get: char
        
        method init = Scanf.Scanning.from_function (fun () -> self#get)
        method fini = next_
    end

let cscanf cf ef fmt rf z =
    let s = cf z in
    let ef0 _ x = ef s x in
    try
        let v = Scanf.kscanf s#init ef0 fmt rf in
        let z = s#fini in
        Some (v, z)
    with
    | No_match ->
        None

class ['cursor] lex_scanner z =
    object
        inherit [char] scanner z
                
        method private get =
            match Lazy.force next_ with
            | Cf_seq.Z ->
                raise End_of_file
            | Cf_seq.P (hd, tl) ->
                next_ <- tl;
                hd
    end

class ['cursor] lex_scanner_x z =
    object
        constraint 'cursor = char #Cf_parser.cursor
        inherit [char * 'cursor] scanner z
    	
        method private get =
            match Lazy.force next_ with
            | Cf_seq.Z ->
                raise End_of_file
            | Cf_seq.P ((ch, _), tl) ->
                next_ <- tl;
                ch
    end

let scanf fmt rf z =
    let ef _ = raise No_match in
    cscanf (new lex_scanner) ef fmt rf z

let scanfx fmt rf z =
    let ef _ = raise No_match in
    cscanf (new lex_scanner_x) ef fmt rf z

(*--- End of File [ cf_scan_parser.ml ] ---*)
