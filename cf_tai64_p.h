/*---------------------------------------------------------------------------*
  C HEADER  cf_tai64_p.h

  Copyright (c) 2003-2006, James H. Woodyatt
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE. 
 *---------------------------------------------------------------------------*/

#ifndef _CF_TAI64_P_H
#define _CF_TAI64_P_H

#include "cf_common_p.h"

#if !defined(ARCH_INT64_TYPE)
#error 64-bit arithmetic not supported by C compiler (fix!)
#else
#endif

#define CF_TAI64_EPOCH		0x4000000000000000ULL
#define CF_TAI64_MJD_EPOCH	0x3fffffff2efbbf8aULL

struct cf_tai64_s { uint64 s; };
typedef struct cf_tai64_s Cf_tai64_t;

#define Cf_tai64_val(v)  ((Cf_tai64_t*) Data_custom_val(v))

extern void cf_tai64_range_error(void);
extern void cf_tai64_label_error(void);

extern value cf_tai64_alloc(const Cf_tai64_t* tai64Ptr);
extern void cf_tai64_update(Cf_tai64_t* tai64Ptr);

extern int cf_tai64_current_offset;

#endif /* defined(_CF_TAI64_P_H) */

/*--- End of File [ cf_tai64_p.h ] ---*/
